package com.ipan;

import com.google.common.base.Stopwatch;
import org.bitcoinj.core.Utils;
import org.bitcoinj.crypto.MnemonicCode;
import org.bitcoinj.crypto.MnemonicException;
import org.bitcoinj.crypto.PBKDF2SHA512;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class MnemonicCodeEx extends MnemonicCode {


    private static final int PBKDF2_ROUNDS = 2048;

    public MnemonicCodeEx() throws IOException {
        super();
    }

    public MnemonicCodeEx(InputStream wordstream, String wordListDigest) throws IOException, IllegalArgumentException {
        super(wordstream, wordListDigest);
    }


    public BigInteger toElectrumEntropy(List<String> words) {

        ArrayList<String> reversedWords = new ArrayList<>();
        reversedWords.addAll(words);
        Collections.reverse(reversedWords);

        List<String> wordList = this.getWordList();
        int wordListSize = wordList.size();

        BigInteger wls = BigInteger.valueOf(wordListSize);
        BigInteger entropy = BigInteger.valueOf(0);

        for (String word : reversedWords) {
            int index = Collections.binarySearch(wordList, word);
            entropy = entropy.multiply(wls).add(BigInteger.valueOf(index));
        }
        return entropy;
    }


    public String toElectrumMnemonic(BigInteger i) {

        List<String> wordList = this.getWordList();
        BigInteger n = BigInteger.valueOf(wordList.size());
        List<String> words = new ArrayList<>();

        while (!Objects.equals(i, BigInteger.valueOf(0))) {
            BigInteger x = i.mod(n);
            i = i.divide(n);
            words.add(wordList.get(x.intValue()));
        }
        return words.toString();
    }

    /**
     * Convert mnemonic word list to seed.
     */
    public static byte[] toSeed(List<String> words, String passphrase) {

        String pass = Utils.join(words);
        String salt = "electrum" + passphrase;

        final Stopwatch watch = Stopwatch.createStarted();
        byte[] seed = PBKDF2SHA512.derive(pass, salt, PBKDF2_ROUNDS, 64);
        watch.stop();
        return seed;
    }

}
