package com.ipan;

import com.google.common.base.Charsets;
import org.bitcoinj.core.Utils;
import org.bitcoinj.crypto.MnemonicException;
import org.bitcoinj.wallet.DeterministicSeed;
import org.bitcoinj.wallet.Protos;

import javax.annotation.Nullable;

import static org.bitcoinj.core.Utils.HEX;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class DeterministicSeedEx extends DeterministicSeed {
    @Nullable
    private final byte[] seed;
    @Nullable
    private final List<String> mnemonicCode;


    public DeterministicSeedEx(List<String> mnemonicCode, byte[] seed) {
        super(new byte[0], new ArrayList<>(), 0l);
        this.seed = seed;
        this.mnemonicCode = mnemonicCode;
    }


    public String toHexString() {
        return seed != null ? HEX.encode(seed) : null;
    }

    @Nullable
    public byte[] getSeedBytes() {
        return seed;
    }

    @Override
    public Protos.Wallet.EncryptionType getEncryptionType() {
        return Protos.Wallet.EncryptionType.ENCRYPTED_SCRYPT_AES;
    }

}
