package com.ipan;

import com.google.common.collect.ImmutableList;
import org.bitcoinj.crypto.*;
import org.bitcoinj.wallet.BasicKeyChain;
import org.bitcoinj.wallet.DeterministicKeyChain;
import org.bitcoinj.wallet.DeterministicSeed;

import javax.annotation.Nullable;
import java.security.SecureRandom;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.bitcoinj.wallet.DeterministicKeyChain.ACCOUNT_ZERO_PATH;

public class DeterministicKeyChainEx extends DeterministicKeyChain {

    public DeterministicKeyChainEx(DeterministicSeed seed) {
        super(seed);
    }

    @Override
    protected ImmutableList<ChildNumber> getAccountPath() {
        return  ImmutableList.of();
    }
}