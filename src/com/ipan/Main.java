package com.ipan;

import org.bitcoinj.core.*;
import org.bitcoinj.crypto.DeterministicKey;
import org.bitcoinj.params.MainNetParams;
import org.bitcoinj.wallet.*;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

public class Main {

    public static void main(String[] args) throws Exception {

        NetworkParameters params = MainNetParams.get();

        String phrase = "double panda veteran endorse provide age woman flame shrug then squirrel rival";
//         todo: you need this          ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
        DeterministicKeyChainEx chain = DeterministicKeyChainService.get(phrase, "");
//         todo: you need this          ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

        List<DeterministicKey> keys = chain.getKeys(KeyChain.KeyPurpose.RECEIVE_FUNDS, 100);

        for (DeterministicKey key : keys) {
            System.out.println(key.toAddress(params));
        }
    }
}

