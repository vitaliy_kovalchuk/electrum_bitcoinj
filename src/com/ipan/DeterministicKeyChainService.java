package com.ipan;

import org.bitcoinj.crypto.MnemonicCode;
import org.bitcoinj.crypto.MnemonicException;

import java.util.List;

public class DeterministicKeyChainService {

    private static DeterministicKeyChainEx get(List<String> phrase, String passphrase, boolean isElectrumSeed) {

        byte[] seed;

        if (isElectrumSeed) {
            seed = MnemonicCodeEx.toSeed(phrase, passphrase);
        } else {
            seed = MnemonicCode.toSeed(phrase, passphrase);
        }

        DeterministicSeedEx seedEx = new DeterministicSeedEx(phrase, seed);
        return new DeterministicKeyChainEx(seedEx);
    }

    private static boolean isElectrumSeed(List<String> phrase, String passphrase) {
        try {
            MnemonicCode.INSTANCE.toEntropy(phrase);
            return false;
        } catch (MnemonicException.MnemonicLengthException | MnemonicException.MnemonicWordException | MnemonicException.MnemonicChecksumException e) {
            return true;
        }
    }

    public static DeterministicKeyChainEx get(String phrase, String passphrase) {
        return DeterministicKeyChainService.get(List.of(phrase.split(" ")), passphrase);
    }

    public static DeterministicKeyChainEx get(List<String> phrase, String passphrase) {
        boolean isElectrumSeed = DeterministicKeyChainService.isElectrumSeed(phrase, passphrase);
        return DeterministicKeyChainService.get(phrase, passphrase, isElectrumSeed);
    }

}
